package org.levelup.stack;

import org.junit.jupiter.api.*;

public class ClassTest {

    @BeforeAll
    public static void once() {
        System.out.println("I'm running first...");
    }

    @BeforeEach
    public void setup() {
        System.out.println("Before each test...");
    }

    @Test
    public void test() {
        System.out.println("My first test...");
    }

    @AfterEach
    public void clear() {
        System.out.println("After each test...");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("I'm the last...");
    }
}
