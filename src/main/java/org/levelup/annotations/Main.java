package org.levelup.annotations;

public class Main {

    // System.getProperty("user.dir")
    public static void main(String[] args) throws IllegalAccessException {
        RussianRoulette roulette = new RussianRoulette();
        RandomIntAnnotationProcessor.setField(roulette);
        System.out.println(roulette.getNumber());
        roulette.guess(4);
    }

}
